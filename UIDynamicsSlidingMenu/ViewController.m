#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UICollisionBehavior *collisionBehaviour;
@property (nonatomic, strong) UIGravityBehavior *gravityBehaviour;
@property (nonatomic, strong) UIPushBehavior *pushBehavior;
@property (nonatomic, strong) UIDynamicItemBehavior *itemBehaviour;

@property (strong, nonatomic) UIPanGestureRecognizer *panRecognizer;
@end

@implementation ViewController {
    BOOL settingsOpen;
    BOOL matchesOpen;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"UIDynamics Sliding Menu";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"] style:UIBarButtonItemStylePlain target:self action:@selector(openSettings)];
	self.navigationItem.leftBarButtonItem.tintColor = [UIColor redColor];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bubble.png"] style:UIBarButtonItemStylePlain target:self action:@selector(openMatches)];
	self.navigationItem.rightBarButtonItem.tintColor = [UIColor redColor];
    
    [self.panRecognizer setMaximumNumberOfTouches:1];
    
    settingsOpen = NO;
    matchesOpen = NO;
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePanel:)];
    [self.navigationController.view addGestureRecognizer:self.panRecognizer];
    [self setupAnimatorProperties];
}

-(void)setupAnimatorProperties {
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:[[UIApplication sharedApplication] keyWindow]];
    
    self.collisionBehaviour = [[UICollisionBehavior alloc] initWithItems:@[self.navigationController.view]];
    [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, -200, 0, -200)];

    self.gravityBehaviour = [[UIGravityBehavior alloc] initWithItems:@[self.navigationController.view]];
    
    self.pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.navigationController.view] mode:UIPushBehaviorModeInstantaneous];
    self.pushBehavior.magnitude = 0.0f;
    self.pushBehavior.angle = 0.0f;
    
    self.itemBehaviour = [[UIDynamicItemBehavior alloc] initWithItems:@[self.navigationController.view]];
    self.itemBehaviour.elasticity = 0.25f;
}

- (void)openSettings {
    [self.animator removeAllBehaviors];
    if(!settingsOpen) {
        [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, 0, 0, -268)];
        self.gravityBehaviour.gravityDirection = CGVectorMake(1, 0);
        self.pushBehavior.pushDirection = CGVectorMake(150.0f, 0.0f);
        settingsOpen = YES;
    }else {
        [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, 0, 0, 800)];
        self.gravityBehaviour.gravityDirection = CGVectorMake(-1, 0);
        self.pushBehavior.pushDirection = CGVectorMake(-150.0f, 0.0f);
        settingsOpen = NO;
    }
    [self.animator addBehavior:self.collisionBehaviour];
    [self.animator addBehavior:self.pushBehavior];
    [self.animator addBehavior:self.gravityBehaviour];
    [self.animator addBehavior:self.itemBehaviour];
    self.pushBehavior.active = YES;
}

- (void)openMatches {
    [self.animator removeAllBehaviors];
    
    if(!matchesOpen) {
        [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, -268, 0, 0)];
        self.gravityBehaviour.gravityDirection = CGVectorMake(-1, 0);
        self.pushBehavior.pushDirection = CGVectorMake(-150.0f, 0.0f);
        matchesOpen = YES;
    }else {
        [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, 800, 0, 0)];
        self.gravityBehaviour.gravityDirection = CGVectorMake(1, 0);
        self.pushBehavior.pushDirection = CGVectorMake(150.0f, 0.0f);
        matchesOpen = NO;
    }
    [self.animator addBehavior:self.collisionBehaviour];
    [self.animator addBehavior:self.pushBehavior];
    [self.animator addBehavior:self.gravityBehaviour];
    [self.animator addBehavior:self.itemBehaviour];
    self.pushBehavior.active = YES;
}

-(void)movePanel:(UIPanGestureRecognizer *)gestureRecognizer {
    CGPoint translation = [gestureRecognizer translationInView:self.view];
    
	gestureRecognizer.view.center = CGPointMake(gestureRecognizer.view.center.x + translation.x, gestureRecognizer.view.center.y);
	[gestureRecognizer setTranslation:CGPointMake(0, 0) inView:gestureRecognizer.view];
    
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self.animator removeAllBehaviors];
        settingsOpen = NO;
        matchesOpen = NO;
        
		
    }else if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        
    }else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint velocity = [gestureRecognizer velocityInView:self.view];
        
        
        if(self.navigationController.view.frame.origin.x <= 160 && self.navigationController.view.frame.origin.x >= 0) {
            if(velocity.x >= 200) {
                [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, 0, 0, -268)];
                [self.animator addBehavior:self.collisionBehaviour];
                self.gravityBehaviour.gravityDirection = CGVectorMake(1, 0);
                settingsOpen = YES;
            }else {
                [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, 0, 0, 800)];
                [self.animator addBehavior:self.collisionBehaviour];
                self.gravityBehaviour.gravityDirection = CGVectorMake(-1, 0);
                settingsOpen = NO;
            }
            matchesOpen = NO;
        }else if(self.navigationController.view.frame.origin.x >= -160 && self.navigationController.view.frame.origin.x <= 0) {
            if(velocity.x <= -200) {
                [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, -268, 0, 0)];
                [self.animator addBehavior:self.collisionBehaviour];
                self.gravityBehaviour.gravityDirection = CGVectorMake(-1, 0);
                matchesOpen = YES;
            }else {
                [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, 800, 0, 0)];
                [self.animator addBehavior:self.collisionBehaviour];
                self.gravityBehaviour.gravityDirection = CGVectorMake(1, 0);
                matchesOpen = NO;
            }
            settingsOpen = NO;
        }else if (self.navigationController.view.frame.origin.x >= 160) {
            if(velocity.x <= -200) {
                [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, 0, 0, 800)];
                [self.animator addBehavior:self.collisionBehaviour];
                self.gravityBehaviour.gravityDirection = CGVectorMake(-1, 0);
                settingsOpen = NO;
            }else {
                [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, 0, 0, -268)];
                [self.animator addBehavior:self.collisionBehaviour];
                self.gravityBehaviour.gravityDirection = CGVectorMake(1, 0);
                settingsOpen = YES;
            }
            matchesOpen = NO;
        }else if (self.navigationController.view.frame.origin.x <= -160) {
            if(velocity.x >= 200) {
                [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, 800, 0, 0)];
                [self.animator addBehavior:self.collisionBehaviour];
                self.gravityBehaviour.gravityDirection = CGVectorMake(1, 0);
                matchesOpen = NO;
            }else {
                [self.collisionBehaviour setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, -268, 0, 0)];
                [self.animator addBehavior:self.collisionBehaviour];
                self.gravityBehaviour.gravityDirection = CGVectorMake(-1, 0);
                matchesOpen = YES;
            }
            settingsOpen = NO;
        }
        
        [self.animator addBehavior:self.gravityBehaviour];
        self.pushBehavior.pushDirection = CGVectorMake(velocity.x / 10, 0);
        self.pushBehavior.active = YES;
        [self.animator addBehavior:self.pushBehavior];
    }
    
}

@end

# A sliding navigation menu that utilizes apple's UIDynamics API.  Dynamic behaviors offer a way to improve the user experience of your app by incorporating real-world behavior and characteristics.  This a rough draft of the menu that has been optimized much better in the Cosmos prototype.

# Things needing to be fixed:
# 1.  Incorporate updateItemUsingCurrentState as opposed to removing behaviors on every state change.
# 2.  Add boundary identifiers and collision delegates so we can monitor when the menu has changed positions etc..

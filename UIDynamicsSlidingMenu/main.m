//
//  main.m
//  UIDynamicsSlidingMenu
//
//  Created by Pair Station Foxtrot on 4/10/14.
//  Copyright (c) 2014 Pair Station Foxtrot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IMVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IMVAppDelegate class]));
    }
}

#import <UIKit/UIKit.h>

@interface IMVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
